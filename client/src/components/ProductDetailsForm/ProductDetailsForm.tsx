import { useDispatch } from 'react-redux';
import { CartPayloadInterface, CART_ACTIONS } from '../../store/cart-store';
import { useSelectInput } from '../../hooks/use-input';
import { Product, ProductDetailsPageText } from '../../models';
import classes from './ProductDetailsForm.module.css';
import { NavLink } from 'react-router-dom';

const ProductDetailsForm: React.FC<{
  currentProduct: Product | null;
  textMap: ProductDetailsPageText | null;
}> = ({ currentProduct, textMap }) => {
  const dispatch = useDispatch();

  const {
    selectInputValue: selectInputValueMen,
    errorMessage: errorMessageMen,
    isSelectValid: isSelectValidMen,
    handleSelect: handleSelectMen,
  } = useSelectInput('', (val: string) => {
    if (!val) {
      return ['Please select a size'];
    }
    return null;
  });

  const {
    selectInputValue: selectInputValueWomen,
    errorMessage: errorMessageWomen,
    isSelectValid: isSelectValidWomen,
    handleSelect: handleSelectWomen,
  } = useSelectInput('', (val: string) => {
    if (!val) {
      return ['Please select a size'];
    }
    return null;
  });

  const handleSizeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value;
    if (value !== 'default') {
      const [_, __, sex] = value.split('_');
      if (sex.toLowerCase() === 'men') {
        handleSelectMen(e);
      } else if (sex.toLowerCase() === 'women') {
        handleSelectWomen(e);
      }
    }
    return;
  };

  const addItemToCart = (sex: string) => {
    let sku: string = '';
    let size: string = '';
    if (sex.toLowerCase() === 'men') {
      if (!selectInputValueMen) return;
      [sku, size] = selectInputValueMen.split('_');
    } else if (sex.toLowerCase() === 'women') {
      if (!selectInputValueWomen) return;
      [sku, size] = selectInputValueWomen.split('_');
    }

    const payload: CartPayloadInterface = {
      sku,
      item: {
        priceUsd: currentProduct!.priceUsd,
        tinyImageUrl: currentProduct!.tinyImageUrl,
        sex,
        size,
      },
    };

    dispatch({ type: CART_ACTIONS.ADD, payload });
  };

  return (
    <form>
      {currentProduct!.sizeUsMen.length > 0 && (
        <>
          <div className="form-input-container">
            <label htmlFor="size-us-men">
              {textMap ? textMap.sex_men : "Men's Sizes"}
            </label>
            <select
              id="size-us-men"
              name="size-us-men"
              onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                handleSizeChange(e);
              }}
            >
              <option value="default">
                {textMap
                  ? textMap.product_details_form.inputs.placeholder_text
                  : 'Please Select a Size:'}
              </option>
              {currentProduct!.sizeUsMen.map((size: string) => (
                <option
                  value={`${currentProduct!.sku}_${size}_men`}
                  key={`${currentProduct!.sku}_${size}_men`}
                >
                  {size}
                </option>
              ))}
            </select>
          </div>
          <div className={classes['qty-button-container']}>
            <button
              type="button"
              onClick={() => {
                addItemToCart('men');
              }}
            >
              {textMap
                ? textMap.product_details_form.buttons.add
                : '+ Add to Cart'}
            </button>
          </div>
        </>
      )}
      {currentProduct!.sizeUsWomen.length > 0 && (
        <>
          <div className="form-input-container">
            <label htmlFor="size-us-women">
              {textMap
                ? textMap.product_details_form.labels.women
                : "Women's Sizes"}
            </label>
            <select
              id="size-us-women"
              name="size-us-women"
              onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                handleSizeChange(e);
              }}
            >
              <option value="default">
                {textMap
                  ? textMap.product_details_form.inputs.placeholder_text
                  : 'Please Select a Size:'}
              </option>
              {currentProduct!.sizeUsWomen.map((size: string) => (
                <option
                  value={`${currentProduct!.sku}_${size}_women`}
                  key={`${currentProduct!.sku}_${size}_women`}
                >
                  {size}
                </option>
              ))}
            </select>
          </div>
          <div className={classes['qty-button-container']}>
            <button
              type="button"
              onClick={() => {
                addItemToCart('women');
              }}
            >
              {textMap
                ? textMap.product_details_form.buttons.add
                : '+ Add to Cart'}
            </button>
          </div>
        </>
      )}
      <div className="form-controls-container">
        <NavLink to="/">
          {textMap
            ? textMap.product_details_form.links.home
            : 'Back to Products'}
        </NavLink>
        <button type="submit">
          {textMap ? textMap.product_details_form.buttons.checkout : 'Checkout'}
        </button>
      </div>
    </form>
  );
};

export default ProductDetailsForm;
