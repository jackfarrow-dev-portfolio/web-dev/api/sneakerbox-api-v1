import { useDispatch, useSelector } from 'react-redux';
import {
  CART_ACTIONS,
  CartPayloadInterface,
  CartStateInterface,
} from '../../store/cart-store';
import classes from './Cart.module.css';

interface CartItemDisplayInterface {
  tinyImageUrl: string;
  priceUsd: number;
  men?: { size: string; qty: number }[];
  women?: { size: string; qty: number }[];
  totalQty: number;
  sku: string;
}
const Cart: React.FC<{ subtotalLabel: string }> = ({ subtotalLabel }) => {
  const cart = useSelector((state: CartStateInterface) => state.cart);
  const size = useSelector((state: CartStateInterface) => state.size);
  const subtotal = useSelector((state: CartStateInterface) => state.subtotal);
  const dispatch = useDispatch();

  function displayCartContents() {
    const cartAsArray: CartItemDisplayInterface[] = [];
    Object.keys(cart).map((key: string) => {
      const cartItem = cart[key];
      let men: { size: string; qty: number }[] = [];
      let women: { size: string; qty: number }[] = [];

      if (
        (!cartItem.men || cartItem.men.length === 0) &&
        (!cartItem.women || cartItem.women.length === 0)
      ) {
        let foundIdx = cartAsArray.findIndex(
          (c: CartItemDisplayInterface) =>
            c.tinyImageUrl === cartItem.tinyImageUrl
        );
        if (foundIdx !== -1) {
          cartAsArray.filter(
            (c: CartItemDisplayInterface) =>
              c.tinyImageUrl !== cartItem.tinyImageUrl
          );
        }
      } else {
        if (cartItem.men) {
          men = Object.keys(cartItem.men).map((size: string) => {
            return { size: size, qty: cartItem.men![size] };
          });
        }

        if (cartItem.women) {
          women = Object.keys(cartItem.women).map((size: string) => {
            return { size: size, qty: cartItem.women![size] };
          });
        }

        cartAsArray.push({
          tinyImageUrl: cartItem.tinyImageUrl,
          priceUsd: cartItem.priceUsd,
          men,
          women,
          totalQty: men.length + women.length,
          sku: key,
        });
      }
    });
    return cartAsArray;
  }

  function removeItemFromCartHandler(
    sku: string,
    size: string,
    sex: string,
    priceUsd: number
  ) {
    const payload: CartPayloadInterface = {
      sku,
      item: {
        sex,
        size,
        priceUsd,
      },
    };
    dispatch({ type: CART_ACTIONS.REMOVE, payload });
  }

  function addItemToCartHandler(
    sku: string,
    size: string,
    sex: string,
    priceUsd: number
  ) {
    const payload: CartPayloadInterface = {
      sku,
      item: {
        sex,
        size,
        priceUsd,
      },
    };
    dispatch({ type: CART_ACTIONS.ADD, payload });
  }

  return (
    <div className={classes.cart}>
      {size === 0 && <p>No Items in Cart!</p>}
      {size > 0 && (
        <>
          <p>Items: {size}</p>
          <div className={classes['cart-contents']}>
            {displayCartContents().map((item: CartItemDisplayInterface) => {
              return (
                <div key={item.tinyImageUrl}>
                  <img
                    alt={item.tinyImageUrl}
                    src={`http://localhost:8001/${item.tinyImageUrl}`}
                  />
                  <p>Price: {`$${item.priceUsd}`}</p>
                  {item.men &&
                    item.men.map((i: { qty: number; size: string }) => {
                      return (
                        <div key={`item-men-${i.size}-${item.tinyImageUrl}`}>
                          <p>{`Size: ${i.size} (Men) Quantity: ${i.qty}`}</p>
                          <button
                            type="button"
                            onClick={() => {
                              removeItemFromCartHandler(
                                item.sku,
                                i.size,
                                'men',
                                item.priceUsd
                              );
                            }}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => {
                              addItemToCartHandler(
                                item.sku,
                                i.size,
                                'men',
                                item.priceUsd
                              );
                            }}
                          >
                            +
                          </button>
                        </div>
                      );
                    })}
                  {item.women &&
                    item.women.map((i: { qty: number; size: string }) => {
                      return (
                        <div key={`item-women-${i.size}-${item.tinyImageUrl}`}>
                          <p>{`Size: ${i.size} (Women) Quantity: ${i.qty}`}</p>
                          <button
                            type="button"
                            onClick={() => {
                              removeItemFromCartHandler(
                                item.sku,
                                i.size,
                                'women',
                                item.priceUsd
                              );
                            }}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => {
                              addItemToCartHandler(
                                item.sku,
                                i.size,
                                'women',
                                item.priceUsd
                              );
                            }}
                          >
                            +
                          </button>
                        </div>
                      );
                    })}
                  {/* <h3>
                    {size} X ${item.priceUsd} = ${size * item.priceUsd}
                  </h3> */}
                </div>
              );
            })}
          </div>
        </>
      )}
      <h3>
        {subtotalLabel}: ${subtotal}
      </h3>
    </div>
  );
};

export default Cart;
