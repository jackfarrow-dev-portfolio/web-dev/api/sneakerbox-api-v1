import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { ProductDetailsForm } from '..';
import {
  fetchProductByProductSku,
  fetchProductDescriptionBySkuAndLanguage,
  fetchProductDetailsPageTextByLanguage,
} from '../../services';
import {
  HttpResponse,
  Product,
  ProductDescription,
  ProductDetailsPageText,
} from '../../models';
import { i18nStoreInterface } from '../../store/i18n-store';
import classes from './ProductDetails.module.css';

const ProductDetails: React.FC<{}> = ({}) => {
  const params = useParams();
  const [currentProduct, setCurrentProduct] = useState<Product | null>(null);
  const [isProductFetchPending, setIsProductFetchPending] =
    useState<boolean>(true);
  const [isProductFetchSuccess, setIsProductFetchSuccess] =
    useState<boolean>(false);
  const preferredLanguage = useSelector(
    (state: i18nStoreInterface) => state.preferredLanguage
  );
  const [description, setDescription] = useState<string>('');
  const [productDetailsTextMap, setProductDetailsTextMap] =
    useState<ProductDetailsPageText | null>(null);

  useEffect(() => {
    async function fetchSneakerBySku() {
      const response = await fetchProductByProductSku(params.id!);
      if (response) setIsProductFetchPending(false);
      if (response.ok) {
        setIsProductFetchSuccess(true);
        setCurrentProduct(response.body);
      } else if (!response.ok) {
        setIsProductFetchSuccess(false);
      }
    }
    fetchSneakerBySku();
  }, []);

  useEffect(() => {
    async function fetchDescriptionByLanguage() {
      const descriptionResponse: HttpResponse<ProductDescription[]> =
        await fetchProductDescriptionBySkuAndLanguage(
          params.id!,
          preferredLanguage
        );
      if (descriptionResponse.ok) {
        if (descriptionResponse && descriptionResponse.body) {
          setDescription(descriptionResponse.body[0].description);
        }
      }
    }

    async function fetchProductDetailsTextByLanguage() {
      const detailsResponse: HttpResponse<ProductDetailsPageText> =
        await fetchProductDetailsPageTextByLanguage(preferredLanguage);
      if (detailsResponse.ok) {
        if (detailsResponse && detailsResponse.body) {
          setProductDetailsTextMap(detailsResponse!.body);
        }
      }
    }

    fetchDescriptionByLanguage();
    fetchProductDetailsTextByLanguage();
  }, [preferredLanguage]);

  return (
    <div className={classes['product-details']}>
      {isProductFetchPending && <h2>Fetching product...</h2>}
      {!isProductFetchPending && !isProductFetchSuccess && (
        <h2>Failed to fetch product.</h2>
      )}
      {!isProductFetchPending && isProductFetchSuccess && (
        <>
          <img
            alt={currentProduct!.model}
            src={`http://localhost:8001/${currentProduct!.imageUrl}`}
          />
          <h2>{currentProduct!.model}</h2>
          <p className={classes.description}>{description}</p>
          <p>
            {currentProduct!.sex === 'Unisex'
              ? 'Men & Women'
              : currentProduct!.sex}
          </p>
          <ProductDetailsForm
            currentProduct={currentProduct}
            textMap={productDetailsTextMap}
          />
        </>
      )}
    </div>
  );
};

export default ProductDetails;
