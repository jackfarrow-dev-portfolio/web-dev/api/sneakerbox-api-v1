import { useEffect, useState } from 'react';
import { fetchAllProducts } from '../../services';
import { HttpResponse, Product } from '../../models';
import { ProductThumbnail } from '..';
import classes from './Products.module.css';

const Products: React.FC<{}> = ({}) => {
  const [productsList, setProductsList] = useState<Product[]>([]);
  const [productFetchPending, setProductFetchPending] = useState<boolean>(true);
  const [productFetchSuccess, setProductFetchSuccess] =
    useState<boolean>(false);

  useEffect(() => {
    async function fetchProducts() {
      const products: HttpResponse<Product[]> = await fetchAllProducts();
      if (products.status < 400) {
        setProductsList(products.body!);
        setProductFetchPending(false);
        setProductFetchSuccess(true);
      } else {
        setProductFetchPending(false);
        setProductFetchSuccess(false);
      }
    }
    fetchProducts();
  }, []);

  return (
    <>
      {productFetchPending && !productFetchSuccess && (
        <i>Fetching Products...</i>
      )}
      {!productFetchPending && !productFetchSuccess && (
        <i>Failed to Fetch Products</i>
      )}
      {!productFetchPending && productFetchSuccess && (
        <div className={classes['products-container']}>
          <h3>Products</h3>
          <div className={classes['products-list']}>
            {productsList.map((p: Product) => (
              <ProductThumbnail key={p.sku} sneaker={p} />
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default Products;
