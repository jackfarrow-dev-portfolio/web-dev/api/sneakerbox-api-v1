import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { NavLink, useSearchParams } from 'react-router-dom';
import { Cart, LanguageSelector } from '..';
import { i18nStoreInterface } from '../../store/i18n-store';
import { fetchHomePageTextByLanguage } from '../../services/i18n-service';
import classes from './Header.module.css';
import HomePageText from '../../models/HomePageText';
import { HttpResponse } from '../../models';

const Header: React.FC<{}> = ({}) => {
  const preferredLanguage = useSelector(
    (state: i18nStoreInterface) => state.preferredLanguage
  );
  const [searchParams, _] = useSearchParams();
  const [subheaderText, setSubheaderText] = useState<string>(
    "Bringing You The World's Finest Footwear Since 2024"
  );
  const [homeText, setHomeText] = useState<string>('Home');
  const [loginText, setLoginText] = useState<string>('Login');
  const [subtotalText, setSubtotalText] = useState<string>(
    'Subtotal Before Taxes and Shipping'
  );
  const [languageLabelText, setLanguageLabelText] = useState<string>(
    'Select Your Preferred Language'
  );
  const isLoginMode = searchParams.get('mode') === 'login';

  useEffect(() => {
    async function fetchHomePageText() {
      const homePageTextResponse: HttpResponse<HomePageText> =
        await fetchHomePageTextByLanguage(preferredLanguage);
      if (homePageTextResponse.ok) {
        const {
          subheader,
          navigation: { home, login },
          cart: { label, subtotal },
          homepage: { banner },
        } = homePageTextResponse.body!;
        setSubheaderText(subheader);
        setHomeText(home);
        setLoginText(login);
        setSubtotalText(subtotal);
        setLanguageLabelText(label);
      }
    }
    fetchHomePageText();
  }, [preferredLanguage]);

  return (
    <header className={classes.header}>
      <h1>Sneakerboxx</h1>
      <h2>{subheaderText}</h2>
      <NavLink
        to="/"
        className={({ isActive }) => (isActive ? 'active' : '')}
        end
      >
        {homeText}
      </NavLink>
      <NavLink
        to={isLoginMode ? '/auth?mode=signup' : '/auth?mode=login'}
        className={({ isActive }) => (isActive ? 'active' : '')}
      >
        {isLoginMode ? 'Signup' : loginText}
      </NavLink>
      <LanguageSelector label={languageLabelText} />
      <Cart subtotalLabel={subtotalText} />
    </header>
  );
};

export default Header;
