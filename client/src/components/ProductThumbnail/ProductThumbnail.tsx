import { useNavigate } from 'react-router-dom';
import { Product } from '../../models';
import classes from './ProductThumbnail.module.css';

const ProductThumbnail: React.FC<{ sneaker: Product }> = ({ sneaker }) => {
  const navigate = useNavigate();

  const {
    sku,
    model,
    sex,
    sizeUsMen,
    sizeEuMen,
    sizeUsWomen,
    sizeEuWomen,
    tnImageUrl,
    priceUsd,
    priceEu,
  } = sneaker;

  return (
    <div
      className={classes['product-thumbnail']}
      onClick={() => navigate(`/product-details/${sku}`)}
    >
      <div className={classes['bg-img']}>
        <img src={`http://localhost:8001/${tnImageUrl}`} />
      </div>
      <div className={classes['tn-content']}>
        <p className={classes.model}>{model}</p>
        <p>{sex === 'Unisex' ? 'Men & Women' : sex}</p>
        <h3>${priceUsd}</h3>
      </div>
    </div>
  );
};

export default ProductThumbnail;
