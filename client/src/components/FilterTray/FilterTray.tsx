import classes from './FilterTray.module.css';

const FilterTray: React.FC<{}> = ({}) => {
  return (
    <div className={classes['filter-tray']}>
      <h3>Sort and Filter Tray</h3>
    </div>
  );
};

export default FilterTray;
