import { useDispatch } from 'react-redux';
import { I18N_ACTIONS } from '../../store/i18n-store';
import { LANGUAGE_CODES } from '../../utils/i18n-utils';
import classes from './LanguageSelector.module.css';

const LanguageSelector: React.FC<{ label: string }> = ({ label }) => {
  const dispatch = useDispatch();
  return (
    <div className={classes['language-selector']}>
      <span>{label}</span>
      <select
        onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
          dispatch({
            type: I18N_ACTIONS.CHANGE_LANGUAGE,
            payload: { preferredLanguage: e.target.value },
          });
        }}
      >
        {LANGUAGE_CODES.map((lang: { code: string; displayName: string }) => (
          <option value={lang.code} key={`preferred-lang-${lang.code}`}>
            {lang.displayName}
          </option>
        ))}
      </select>
    </div>
  );
};

export default LanguageSelector;
