// import { useNavigate } from 'react-router-dom';
import { Products } from '../../components';
import classes from './HomePage.module.css';

const authToken = localStorage.getItem('authToken') || '';

const HomePage: React.FC<{}> = ({}) => {
  // const navigate = useNavigate();

  // async function getSecret() {
  //   const res = await fetch('http://localhost:8001/auth/secret', {
  //     headers: { Authorization: `Bearer ${authToken}` },
  //   });
  //   console.log(res);
  //   const data = await res.json();
  //   console.log(data);
  //   if (data.status > 399) {
  //     navigate('/auth?mode=login');
  //   }
  // }

  return (
    <div className={classes['home-page']}>
      <h2>Home Page</h2>
      <Products />
    </div>
  );
};

export default HomePage;
