import { ProductDetails } from '../../components';
import classes from './ProductDetailsPage.module.css';

const ProductDetailsPage: React.FC<{}> = ({}) => {
  return (
    <div className={classes['product-details']}>
      <ProductDetails />
    </div>
  );
};

export default ProductDetailsPage;
