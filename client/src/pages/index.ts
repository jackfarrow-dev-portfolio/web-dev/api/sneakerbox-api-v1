import AuthenticationPage from './AuthenticationPage/AuthenticationPage';
import ErrorPage from './ErrorPage/ErrorPage';
import HomePage from './HomePage/HomePage';
import ProductDetailsPage from './ProductDetailsPage/ProductDetailsPage';

export { AuthenticationPage, ErrorPage, HomePage, ProductDetailsPage };
