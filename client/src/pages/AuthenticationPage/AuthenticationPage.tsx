import { useSearchParams } from 'react-router-dom';
import {
  useEmailInput,
  validateEmail,
  usePasswordInput,
  validatePassword,
} from '../../hooks/use-input';
import { login } from '../../services';
import classes from './AuthenticationPage.module.css';

const AuthenticationPage: React.FC<{}> = ({}) => {
  const [searchParams, _] = useSearchParams();
  const isLoginMode = searchParams.get('mode') === 'login';

  const {
    emailInputValue: email,
    // setEmailInputValue: setEmail,
    // isEmailValid,
    handleValueChange: handleEmailValueChange,
  } = useEmailInput('', validateEmail);

  const {
    // errorMessage: passwordError,
    passwordInputValue: password,
    // setPasswordInputValue: setPassword,
    // isPasswordValid,
    handleValueChange: handlePasswordValueChange,
  } = usePasswordInput('', validatePassword);

  async function onSubmit(e: React.FormEvent) {
    e.preventDefault();
    const response = await login(email, password);
    console.log(response);
  }

  return (
    <div className={classes.authentication}>
      <h2>{isLoginMode ? 'Login' : 'Signup'}</h2>
      <form
        onSubmit={(e: React.FormEvent<HTMLElement>) => {
          onSubmit(e);
        }}
      >
        <div className="form-input-container">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              handleEmailValueChange(e);
            }}
          />
        </div>
        <div className="form-input-container">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              handlePasswordValueChange(e);
            }}
          />
        </div>
        <div className="form-controls-container">
          <button type="reset">Cancel</button>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
};

export default AuthenticationPage;
