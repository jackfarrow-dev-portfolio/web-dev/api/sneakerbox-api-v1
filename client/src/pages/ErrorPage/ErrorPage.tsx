import { Link } from 'react-router-dom';
import classes from './ErrorPage.module.css';

const ErrorPage: React.FC<{}> = ({}) => {
  return (
    <div className={classes['error-page']}>
      <h2>An Error Occurred!</h2>
      <Link to="/">Back to Home</Link>
    </div>
  );
};

export default ErrorPage;
