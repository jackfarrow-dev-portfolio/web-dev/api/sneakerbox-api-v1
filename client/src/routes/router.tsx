import { createBrowserRouter } from 'react-router-dom';
import {
  AuthenticationPage,
  ErrorPage,
  HomePage,
  ProductDetailsPage,
} from '../pages';
import { MobileLayout } from '../layouts';

const router = createBrowserRouter([
  {
    path: '/',
    element: <MobileLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: '/',
        element: <HomePage />,
      },
      {
        path: '/auth',
        element: <AuthenticationPage />,
      },
      {
        path: '/product-details/:id',
        element: <ProductDetailsPage />,
      },
    ],
  },
]);

export default router;
