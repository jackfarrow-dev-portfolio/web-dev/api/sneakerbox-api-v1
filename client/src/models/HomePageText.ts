export default interface HomePageText {
  subheader: string;
  navigation: {
    home: string;
    login: string;
  };
  cart: {
    label: string;
    subtotal: string;
  };
  homepage: {
    banner: string;
  };
}
