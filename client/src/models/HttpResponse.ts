export default interface HttpResponse<T> {
  ok: boolean;
  body?: T;
  status: number;
  statusText: string;
  errorMessage?: string;
  bodyUsed: boolean;
}
