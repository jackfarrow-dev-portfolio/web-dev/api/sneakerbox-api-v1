export default interface Product {
  sku: string;
  model: string;
  sex: string;
  sizeUsMen: string[];
  sizeEuMen: string[];
  sizeUsWomen: string[];
  sizeEuWomen: string[];
  colors: string[];
  materials: string[];
  categories: string[];
  countryOfOrigin: string;
  imageUrl: string;
  tnImageUrl: string;
  tinyImageUrl: string;
  priceUsd: number;
  priceEu: number;
}
