export default interface ProdcutDetailsPageText {
  sex_unisex: string;
  sex_men: string;
  sex_women: string;
  product_details_form: {
    labels: {
      men: string;
      women: string;
    };
    inputs: {
      placeholder_text: string;
    };
    buttons: {
      add: string;
      checkout: string;
    };
    links: {
      home: string;
    };
  };
}
