export default interface ProductDescription {
  sku: string;
  description: string;
  model: string;
}
