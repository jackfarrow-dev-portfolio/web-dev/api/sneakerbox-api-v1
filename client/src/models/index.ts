import HomePageText from './HomePageText';
import HttpResponse from './HttpResponse';
import Product from './Product';
import ProductDescription from './ProductDescription';
import ProductDetailsPageText from './ProductDetailsPageText';

export type {
  HomePageText,
  HttpResponse,
  Product,
  ProductDescription,
  ProductDetailsPageText,
};
