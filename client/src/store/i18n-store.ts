import { createStore } from 'redux';
import { ActionInterface } from './cart-store';

export interface i18nStoreInterface {
  preferredLanguage: string;
  country: string;
}

export const initialStore: i18nStoreInterface = {
  preferredLanguage: 'en',
  country: 'US',
};

export const I18N_ACTIONS = {
  CHANGE_LANGUAGE: '[i18n] Change Language',
};

export interface i18nLanguagePayloadInterface {
  preferredLanguage: string;
}

function changePreferredLanguage(
  state: i18nStoreInterface,
  payload: i18nLanguagePayloadInterface
) {
  return { ...state, preferredLanguage: payload.preferredLanguage };
}

const i18nReducer = (
  state: i18nStoreInterface = initialStore,
  action: ActionInterface
) => {
  if (action.type === I18N_ACTIONS.CHANGE_LANGUAGE) {
    return changePreferredLanguage(state, action.payload);
  }
  return { ...state };
};

const store = createStore(i18nReducer);

export default store;
