import { createStore } from 'redux';

export interface CartItemInterface {
  priceUsd: number;
  tinyImageUrl?: string;
  sex: string;
  size: string;
}
/*
{
"abc123-456def-789hij-012klm": {
      priceUsd: 125,
      tinyImageUrl: 'http://fake-img.com/001-tn.jpg,
      sex: 'women',
      size: '8',
      qty: 1
    }
  }
*/
export interface CartStateInterface {
  cart: {
    [key: string]: {
      tinyImageUrl: string;
      priceUsd: number;
      men?: { [key: string]: number };
      women?: { [key: string]: number };
    };
  };
  size: number;
  subtotal: number;
}

/*
  cart: {
    "abc123-456def-789hij-012klm": {
      priceUsd: 125,
      tinyImageUrl: 'http://fake-img.com/001-tn.jpg,
      men: {
        "10.5": 2
      },
      women: {
        "8": 1
      }
    }
  }
*/
const initialState = {
  cart: {},
  size: 0,
  subtotal: 0,
};

export interface ActionInterface {
  type: string;
  payload?: any;
}

export const CART_ACTIONS = {
  ADD: '[Cart] Add Item',
  REMOVE: '[Cart] Remove Item',
  REMOVE_ONE: '[Cart] Remove One',
};

export interface CartPayloadInterface {
  sku: string;
  item: CartItemInterface;
}

export const addItemToCart = { type: CART_ACTIONS.ADD };
export const removeItemFromCart = { type: CART_ACTIONS.REMOVE };

function addToCart(state: CartStateInterface, payload: CartPayloadInterface) {
  const stateCopy = { ...state };

  const {
    sku,
    item: { sex, size, tinyImageUrl, priceUsd },
  } = payload;

  // If sku EXISTS in cart
  if (stateCopy.cart[sku]) {
    let targetSku = stateCopy.cart[sku];
    switch (sex.toLowerCase()) {
      case 'men':
        if (targetSku.men) {
          if (targetSku.men[size]) {
            targetSku.men[size] += 1;
          } else {
            targetSku.men[size] = 1;
          }
        } else {
          targetSku.men = { [size]: 1 };
        }
        stateCopy.subtotal += priceUsd;
        break;
      case 'women':
        if (targetSku.women) {
          if (targetSku.women[size]) {
            targetSku.women[size] += 1;
          } else {
            targetSku.women[size] = 1;
          }
        } else {
          targetSku.women = { [size]: 1 };
        }
        stateCopy.subtotal += priceUsd;
        break;
      default:
        break;
    }
    stateCopy.cart[sku] = targetSku;
  } else {
    // sku does NOT exist in cart
    switch (sex.toLowerCase()) {
      case 'men':
        stateCopy.cart[sku] = {
          priceUsd: priceUsd!,
          tinyImageUrl: tinyImageUrl!,
          men: { [size]: 1 },
          women: {},
        };
        stateCopy.subtotal += priceUsd;
        break;
      case 'women':
        stateCopy.cart[sku] = {
          priceUsd: priceUsd!,
          tinyImageUrl: tinyImageUrl!,
          men: {},
          women: { [size]: 1 },
        };
        stateCopy.subtotal += priceUsd;
        break;
      default:
        break;
    }
  }

  stateCopy.size += 1;
  return stateCopy;
}

function removeFromCart(
  state: CartStateInterface,
  payload: CartPayloadInterface
) {
  const {
    sku,
    item: { size, sex, priceUsd },
  } = payload;

  let stateCopy = { ...state };

  // Nothing to do if sku doesn't exist on state.cart
  if (!stateCopy.cart[sku]) return stateCopy;

  // Sku exists
  let targetSku = stateCopy.cart[sku];
  switch (sex.toLowerCase()) {
    case 'men':
      if (targetSku.men && targetSku.men[size]) {
        if (targetSku.men![size] - 1 > 0) {
          targetSku.men![size] -= 1;
          stateCopy.size -= 1;
        } else if (targetSku.men![size] - 1 === 0) {
          delete targetSku.men![size];
          stateCopy.size -= 1;
        }
      }
      stateCopy.subtotal -= priceUsd;
      break;
    case 'women':
      if (targetSku.women && targetSku.women[size]) {
        if (targetSku.women![size] - 1 > 0) {
          targetSku.women![size] -= 1;
          stateCopy.size -= 1;
        } else if (targetSku.women![size] - 1 === 0) {
          delete targetSku.women![size];
          stateCopy.size -= 1;
        }
      }
      stateCopy.subtotal -= priceUsd;
      break;
    default:
      break;
  }

  if (
    stateCopy.cart[sku].men &&
    Object.keys(stateCopy.cart[sku].men!).length === 0
  ) {
    delete stateCopy.cart[sku].men;
  }

  if (
    stateCopy.cart[sku].women &&
    Object.keys(stateCopy.cart[sku].women!).length === 0
  ) {
    delete stateCopy.cart[sku].women;
  }

  return stateCopy;
}

const cartReducer = (
  state: CartStateInterface = initialState,
  action: ActionInterface
) => {
  if (action.type === CART_ACTIONS.ADD) {
    return addToCart(state, action.payload);
  }
  if (action.type === CART_ACTIONS.REMOVE) {
    return removeFromCart(state, action.payload);
  }
  return state;
};

const store = createStore(cartReducer);

export default store;
