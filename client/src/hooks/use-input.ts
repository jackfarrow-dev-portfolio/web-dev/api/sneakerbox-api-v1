import { useState } from 'react';
import validate from 'validate.js';

export function validateEmail(email: string) {
  const constraints = {
    email: {
      email: true,
    },
  };
  return validate({ email }, constraints);
}

export function useEmailInput(
  initialValue: string,
  validationFn: (val: string) => string[] | null
) {
  const [emailInputValue, setEmailInputValue] = useState<string>(initialValue);
  const errorMessage = validationFn(emailInputValue);
  const isEmailValid = !errorMessage;

  function handleValueChange(event: React.ChangeEvent<HTMLInputElement>) {
    setEmailInputValue(event.target.value);
  }

  return {
    emailInputValue,
    errorMessage,
    handleValueChange,
    isEmailValid,
    setEmailInputValue,
  };
}

export function validatePassword(password: string) {
  const alphabetic = /[AZ-az]/g;
  const numeric = /[0-9]/g;
  const specialChars = /[!@#$%&]/g;

  validate.validators.alphaNumSpecial = function (
    value: string,
    options: any,
    key: string,
    attributes: any
  ) {
    if (
      !alphabetic.test(value) ||
      !numeric.test(value) ||
      !specialChars.test(value)
    ) {
      return `must contain at least one alphabetic value, one numeric value, and one special character: !@#$%&`;
    }
    return undefined;
  };
  const constraints = {
    password: {
      length: {
        minimum: 8,
        tooShort: 'must be at least 8 characters',
      },
      alphaNumSpecial: {},
    },
  };
  return validate({ password }, constraints);
}

export function usePasswordInput(
  initialPasswordValue: string,
  validationFn: (val: string) => string[] | null
) {
  const [passwordInputValue, setPasswordInputValue] =
    useState<string>(initialPasswordValue);

  const errorMessage = validationFn(passwordInputValue);

  const isPasswordValid = !errorMessage;

  function handleValueChange(e: React.ChangeEvent<HTMLInputElement>) {
    setPasswordInputValue(e.target.value);
  }

  return {
    errorMessage,
    handleValueChange,
    isPasswordValid,
    passwordInputValue,
    setPasswordInputValue,
  };
}

export function useSelectInput(
  initialInputValue: string,
  validationFn: (val: string) => string[] | null
) {
  const [selectInputValue, setSelectInputValue] =
    useState<string>(initialInputValue);

  const errorMessage = validationFn(selectInputValue);

  const isSelectValid = !errorMessage;

  const handleSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectInputValue(e.target.value);
  };

  return {
    selectInputValue,
    errorMessage,
    isSelectValid,
    handleSelect,
  };
}
