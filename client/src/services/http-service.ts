import OptionsInterface from './http-options';
import { HttpResponse } from '../models';

export async function httpFetch(
  request: string | Request,
  options?: OptionsInterface
): Promise<Response> {
  let response: Response = await fetch(request, options);
  return response;
}

export async function parseResponse<T>(
  response: Response
): Promise<HttpResponse<T>> {
  const json = await response.json();
  const { ok, status, statusText, bodyUsed } = response;
  const httpResponse: HttpResponse<T> = { ok, status, statusText, bodyUsed };

  if (status <= 399) {
    httpResponse.body = json;
  } else {
    httpResponse.errorMessage = json.message;
  }
  return httpResponse;
}
