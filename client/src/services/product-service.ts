import { httpFetch, parseResponse } from './http-service';
import { HttpResponse, Product } from '../models';
import { API_URL } from '../env/env-config';

export async function fetchAllProducts(): Promise<HttpResponse<Product[]>> {
  const response = await httpFetch(`${API_URL}/sneakers`);
  const httpSneakersData = await parseResponse<Product[]>(response);
  return httpSneakersData;
}

export async function fetchProductByProductSku(
  sku: string
): Promise<HttpResponse<any>> {
  const response = await httpFetch(`${API_URL}/sneakers/find/${sku}`);
  const httpSneakerData = await parseResponse<any>(response);
  return httpSneakerData;
}
