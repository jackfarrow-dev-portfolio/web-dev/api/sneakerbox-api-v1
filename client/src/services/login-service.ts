import OptionsInterface from './http-options';
import { httpFetch, parseResponse } from './http-service';
import HttpResponse from '../models/HttpResponse';
import { API_URL } from '../env/env-config';

export async function login(
  email: string,
  password: string
): Promise<HttpResponse<string>> {
  const options: OptionsInterface = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email, password }),
  };
  const response = await httpFetch(`${API_URL}/auth/login`, options);
  const authTokenResponse: HttpResponse<string> = await parseResponse<string>(
    response
  );
  if (authTokenResponse.status <= 399) {
    localStorage.setItem('authToken', authTokenResponse.body!);
  }
  return authTokenResponse;
}
