import {
  fetchHomePageTextByLanguage,
  fetchProductDescriptionBySkuAndLanguage,
  fetchProductDetailsPageTextByLanguage,
} from './i18n-service';
import { fetchAllProducts, fetchProductByProductSku } from './product-service';
import { login } from './login-service';

export {
  fetchAllProducts,
  fetchHomePageTextByLanguage,
  fetchProductByProductSku,
  fetchProductDescriptionBySkuAndLanguage,
  fetchProductDetailsPageTextByLanguage,
  login,
};
