export default interface OptionsInterface {
  method: string;
  headers?: Headers | { [key: string]: string };
  body?:
    | string
    | Blob
    | ArrayBuffer
    | FormData
    | URLSearchParams
    | ReadableStream;
  mode?: RequestMode;
  credentials?: RequestCredentials;
  cache?: RequestCache;
  redirect?: RequestRedirect;
  referrer?: string;
  referrerPolicy?: ReferrerPolicy;
  integrity?: string;
  keepalive?: boolean;
  priority?: string;
}
