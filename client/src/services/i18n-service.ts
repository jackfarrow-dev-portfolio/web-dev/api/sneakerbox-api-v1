import { httpFetch, parseResponse } from './http-service';
import { API_URL } from '../env/env-config';
import {
  HomePageText,
  HttpResponse,
  ProductDescription,
  ProductDetailsPageText,
} from '../models';

export async function fetchProductDescriptionBySkuAndLanguage(
  sku: string,
  language: string
): Promise<HttpResponse<ProductDescription[]>> {
  const response =
    await httpFetch(`${API_URL}/data/product-desc/${sku}/${language}
  `);
  const httpDescriptionData = await parseResponse<
    { sku: string; description: string; model: string }[]
  >(response);
  return httpDescriptionData;
}

export async function fetchHomePageTextByLanguage(
  language: string
): Promise<HttpResponse<HomePageText>> {
  const response = await httpFetch(`${API_URL}/data/home-page/${language}`);
  const httpHomePageText = await parseResponse<HomePageText>(response);
  return httpHomePageText;
}

export async function fetchProductDetailsPageTextByLanguage(
  language: string
): Promise<HttpResponse<ProductDetailsPageText>> {
  const response = await httpFetch(
    `${API_URL}/data/product-details-page/${language}`
  );
  const httpProductDetailsPageText =
    await parseResponse<ProductDetailsPageText>(response);
  return httpProductDetailsPageText;
}
