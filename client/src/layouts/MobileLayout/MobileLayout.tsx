import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import { Provider } from 'react-redux';
import { FilterTray, Header } from '../../components';
import store from '../../store/i18n-store';

const MobileLayout: React.FC<{}> = ({}) => {
  const [isLoginMode, _] = useState<boolean>(false);

  return (
    <>
      <Provider store={store}>
        <div>
          <Header />
          <FilterTray />
          <Outlet />
        </div>
      </Provider>
    </>
  );
};

export default MobileLayout;
