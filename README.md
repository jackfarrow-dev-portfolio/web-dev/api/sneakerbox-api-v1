# Sneakerboxx-API

## Data Model

`Sneaker`:

| attribute         | type     |
| ----------------- | -------- |
| `PK` sku          | UUID     |
| model             | string   |
| sex               | string   |
| size_us           | string   |
| size_eu           | string   |
| price_us          | number   |
| price_eu          | number   |
| colors            | string[] |
| materials         | string[] |
| categories        | string[] |
| country_of_origin | string   |
| image_url         | string   |
| tn_image_url      | string   |

<hr/>

`Customer`:

| attribute              | type   |
| ---------------------- | ------ |
| `PK` customer_id       | UUID   |
| first_name             | string |
| last_name              | string |
| date_of_birth          | Date   |
| preferred_language     | string |
| email_address          | string |
| username\*             | string |
| password               | string |
| billing_address_1      | string |
| billing_address_2      | string |
| billing_municipality   | string |
| billing_state_province | string |
| billing_country        | string |
| billing_postal_code    | string |

<sup>\*</sup>: _To be deprecated_

<hr/>

`Review`:

| attribute        | type   |
| ---------------- | ------ |
| `PK` review_id   | UUID   |
| `FK` sku         | UUID   |
| `FK` customer_id | UUID   |
| review_text      | string |
| review_dte       | Date   |
| review_rating    |

<hr/>

`Address`:

| attribute           | type    |
| ------------------- | ------- |
| `PK` address_id     | UUID    |
| `FK` customer_id    | UUID    |
| address_line_1      | string  |
| address_line_2      | string  |
| municipality        | string  |
| state_province      | string  |
| country             | string  |
| postal_code         | string  |
| is_shipping_address | boolean |
| is_billing_address  | boolean |

<hr/>

`Purchase`:

| attribute                | type   |
| ------------------------ | ------ |
| `PK` purchase_id         | UUID   |
| `FK` customer_id         | UUID   |
| `FK` payment_method_id   | UUID   |
| `FK` shipping_address_id | UUID   |
| purchase_date            | Date   |
| purchase_currency        | string |
| total_purchase_price     | number |
| purchase_subtotal        | number |
| shipping_cost            | number |
| tax                      | number |

<hr/>

`PurchaseToSneaker`:

| attribute             | type   |
| --------------------- | ------ |
| `PK` sneaker_purchase | UUID   |
| `FK` sku              | UUID   |
| `FK` purchase_id      | UUID   |
| purchase_qty          | number |
| purchase_price        | number |

<hr/>

`PaymentMethod`:

| attribute              | type   |
| ---------------------- | ------ |
| `PK` payment_method_id | UUID   |
| `FK` customer_id       | UUID   |
| card_number            | string |
| expiry_date            | Date   |
| cvv                    | string |

## ERD

![sneakerboxx ERD](./sneakerboxx-erd-5.png)

## API Operations

### Sneakers

- `GET /all`
- `GET /:sku`

### Customers

- `GET /my-details`
- `POST /sign-up`
- `PUT /update-my-details`

### Payment Methods

- `POST /new-payment-method`
- `PUT /update-payment-method-details`
- `DELETE /payment-method`
- `GET /my-payment-methods`

### Reviews

- `GET /all-by/:sku`
- `POST /new`
