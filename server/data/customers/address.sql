DROP TABLE IF EXISTS address;

CREATE TABLE IF NOT EXISTS address (
    address_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    customer_id UUID NOT NULL,
    address_line_1 VARCHAR(75) NOT NULL,
    address_line_2 VARCHAR(75) NOT NULL,
    municipality VARCHAR(75) NOT NULL,
    state_province VARCHAR(75) NOT NULL,
    country VARCHAR(75) NOT NULL,
    postal_code VARCHAR(25) NOT NULL,
    CONSTRAINT fk_customer_address FOREIGN KEY (customer_id) REFERENCES customer(customer_id) ON DELETE CASCADE
);
