DROP TABLE IF EXISTS customer;

CREATE TABLE IF NOT EXISTS customer (
    customer_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    first_name VARCHAR(75) NOT NULL,
    last_name VARCHAR(75) NOT NULL,
    date_of_birth DATE NOT NULL,
    preferred_language VARCHAR(50),
    email_address VARCHAR(125) UNIQUE NOT NULL,
    username VARCHAR(125) UNIQUE NOT NULL,
    password VARCHAR(125) NOT NULL,
    billing_address_line_1 VARCHAR(75) NOT NULL,
    billing_address_line_2 VARCHAR(75),
    billing_municipality VARCHAR(75) NOT NULL,
    billing_state_province VARCHAR(75) NOT NULL,
    billing_country VARCHAR(75) NOT NULL,
    billing_postal_code VARCHAR(25) NOT NULL
);

INSERT INTO customer (first_name, last_name, date_of_birth, preferred_language, username, password, billing_address_line_1, billing_municipality, billing_state_province, billing_country, billing_postal_code, email_address) VALUES ('Johnny', 'Bravo', NOW(), 'Tagalog', 'jbravo123', 'secret', '1234 Main Street', 'Springfield', 'Illinois', 'United States', '12345', 'jbravo123@mail.biz');