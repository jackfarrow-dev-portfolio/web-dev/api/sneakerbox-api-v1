DROP TABLE IF EXISTS review;

CREATE TABLE IF NOT EXISTS review (
    review_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    sku UUID NOT NULL,
    customer_id UUID NOT NULL,
    review_title VARCHAR(125),
    review_text VARCHAR(10000) NOT NULL,
    review_datetime TIMESTAMP DEFAULT NOW(),
    review_rating INT NOT NULL,
    CONSTRAINT fk_review_to_sku FOREIGN KEY(sku) REFERENCES sneaker(sku) ON DELETE CASCADE,
    CONSTRAINT fk_review_to_customer FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

INSERT INTO review (sku, customer_id, review_title, review_text, review_rating) VALUES ('160e1d59-4cb3-442b-908d-f03095700605', '74c9ec90-cbb0-477e-a57a-caf562296cdb', 'My First Review', 'This product is fantastic!', 5);