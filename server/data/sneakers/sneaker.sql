CREATE DATABASE IF NOT EXISTS sneakerboxx;

DROP TABLE IF EXISTS sneaker;

CREATE TABLE IF NOT EXISTS sneaker (
  sku UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  model VARCHAR(75) NOT NULL,
  sex VARCHAR(6) NOT NULL,
  size_us_men TEXT[],
  size_eu_men TEXT[],
  size_us_women TEXT[],
  size_eu_women TEXT[],
  colors TEXT[] NOT NULL,
  materials TEXT[] NOT NULL,
  categories TEXT[],
  country_of_origin VARCHAR(75),
  image_url TEXT NOT NULL,
  tn_image_url TEXT NOT NULL,
  tiny_image_url TEXT NOT NULL,
  price_usd NUMERIC NOT NULL,
  price_eu NUMERIC NOT NULL
);

INSERT INTO sneaker (model, sex, size_us_men, size_eu_men, size_us_women, size_eu_women, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Trainer Low', 'Unisex', '{"6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12"}', '{"40", "41", "42", "43", "44", "45", "46"}', '{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5"}', '{35, 36, 37, 38, 39, 40, 41}', '{"white", "navy", "black"}', '{"leather", "synthetic rubber"}', 'Vietnam', 'static/images/fs_images/sneakers-001.jpg', 'static/images/tn_images/sneakers-001-tn.jpg', 'static/images/tiny_images/sneakers-001-tiny.jpg', 125, 100);

INSERT INTO sneaker (model, sex, size_us_men, size_eu_men, size_us_women, size_eu_women, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Runner Low', 'Unisex', '{"6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12"}', '{"40", "41", "42", "43", "44", "45", "46"}', '{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5"}', '{35, 36, 37, 38, 39, 40, 41}', '{"black", "lime green"}', '{"leather", "synthetic rubber"}', 'Vietnam', 'static/images/fs_images/sneakers-002.jpg', 'static/images/tn_images/sneakers-002-tn.jpg', 'static/images/tiny_images/sneakers-002-tiny.jpg', 125, 100);

INSERT INTO sneaker (model, sex, size_us_men, size_eu_men, size_us_women, size_eu_women, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Classic Chucks Hi-Top', 'Unisex', '{"6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12"}', '{"40", "41", "42", "43", "44", "45", "46"}', '{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5"}', '{35, 36, 37, 38, 39, 40, 41}', '{"red", "white", "blue"}', '{"canvas", "synthetic rubber"}', 'Taiwan', 'static/images/fs_images/sneakers-005.jpg', 'static/images/tn_images/sneakers-005-tn.jpg', 'static/images/tiny_images/sneakers-005-tiny.jpg', 125, 100);

INSERT INTO sneaker (model, sex, size_us_men, size_eu_men, size_us_women, size_eu_women, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Force Low', 'Unisex', '{"6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12"}', '{"40", "41", "42", "43", "44", "45", "46"}', '{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5"}', '{35, 36, 37, 38, 39, 40, 41}', '{"orange", "black", "white"}', '{"leather", "synthetic rubber"}', 'Taiwan', 'static/images/fs_images/sneakers-006.jpg', 'static/images/tn_images/sneakers-006-tn.jpg', 'static/images/tiny_images/sneakers-006-tiny.jpg', 125, 100);

INSERT INTO sneaker (model, sex, size_us_women, size_eu_women, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Athletic Low', 'Women', '{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5"}', '{35, 36, 37, 38, 39, 40, 41}', '{"grey", "lilac", "white"}', '{"leather", "synthetic rubber"}', 'Pakistan', 'static/images/fs_images/sneakers-007.jpg', 'static/images/tn_images/sneakers-007-tn.jpg', 'static/images/tiny_images/sneakers-007-tiny.jpg', 125, 100);

INSERT INTO sneaker (model, sex, size_us_men, size_eu_men, size_us_women, size_eu_women, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Cross Fit Low', 'Unisex', '{"6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12"}', '{"40", "41", "42", "43", "44", "45", "46"}', '{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5"}', '{35, 36, 37, 38, 39, 40, 41}', '{"vermillion", "red", "white"}', '{"leather", "synthetic rubber"}', 'Taiwan', 'static/images/fs_images/sneakers-008.jpg', 'static/images/tn_images/sneakers-008-tn.jpg', 'static/images/tiny_images/sneakers-008-tiny.jpg', 125, 100);

INSERT INTO sneaker (model, sex, size_us_men, size_eu_men, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Ice Low', 'Men', '{"6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12"}', '{"40", "41", "42", "43", "44", "45", "46"}', '{"ice blue", "black", "lime green", "vermillion"}', '{"leather", "synthetic rubber"}', 'Vietnam', 'static/images/fs_images/sneakers-014.jpg', 'static/images/tn_images/sneakers-014-tn.jpg', 'static/images/tiny_images/sneakers-014-tiny.jpg', 100, 125);

INSERT INTO sneaker (model, sex, size_us_men, size_eu_men, size_us_women, size_eu_women, colors, materials, country_of_origin, image_url, tn_image_url, tiny_image_url, price_usd, price_eu) VALUES ('Space Boots High', 'Unisex', '{"6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12"}', '{"40", "41", "42", "43", "44", "45", "46"}', '{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5"}', '{35, 36, 37, 38, 39, 40, 41}', '{"grey", "vermillion", "salmon", "goldenrod"}', '{"leather", "suede", "synthetic rubber"}', 'Taiwan', 'static/images/fs_images/sneakers-015.jpg', 'static/images/tn_images/sneakers-015-tn.jpg', 'static/images/tiny_images/sneakers-015-tiny.jpg', 100, 125);