DROP TABLE IF EXISTS purchase_to_sneaker;

CREATE TABLE purchase_to_sneaker(
    sneaker_purchase_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    sku UUID NOT NULL,
    purchase_id UUID NOT NULL,
    purchase_qty INT NOT NULL,
    purchase_price NUMERIC NOT NULL,
    CONSTRAINT fk_purchase_to_sku FOREIGN KEY(sku) REFERENCES sneaker(sku) ON DELETE CASCADE,
    CONSTRAINT fk_sku_to_purchase FOREIGN KEY(purchase_id) REFERENCES purchase(purchase_id) ON DELETE CASCADE
);