DROP TABLE IF EXISTS payment_method;

CREATE TABLE IF NOT EXISTS payment_method(
    payment_method_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    billing_address_id UUID NOT NULL,
    card_number VARCHAR(18) NOT NULL,
    expiry_date DATE NOT NULL,
    cvv VARCHAR(3) NOT NULL,
    CONSTRAINT fk_payment_method_to_address FOREIGN KEY (billing_address_id) REFERENCES address(address_id)
);