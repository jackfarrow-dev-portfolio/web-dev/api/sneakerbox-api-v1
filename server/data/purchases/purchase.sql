DROP TABLE IF EXISTS purchase;

CREATE TABLE IF NOT EXISTS purchase (
    purchase_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    customer_id UUID NOT NULL,
    payment_method_id UUID NOT NULL,
    shipping_address_id UUID NOT NULL,
    purchase_date DATE NOT NULL,
    purchase_currency VARCHAR(25) NOT NULL,
    total_purchase_price NUMERIC NOT NULL,
    purchase_subtotal NUMERIC NOT NULL,
    shipping_costs NUMERIC NOT NULL,
    taxes NUMERIC NOT NULL,
    CONSTRAINT fk_purchase_to_customer FOREIGN KEY(customer_id) REFERENCES customer(customer_id),
    CONSTRAINT fk_purchase_to_payment_method FOREIGN KEY(payment_method_id) REFERENCES payment_method(payment_method_id)
);
