import { QueryResult } from 'pg';
import { getClient } from '../dbclient/db-client';
import { ReviewInterface } from './review.entity';
import { BadRequestErr, ServerErr } from '../errors/http-errors';

export async function getReviewsForSku(sku: string) {
  const client = getClient();
  await client.connect();

  client.on('error', (err: Error) => {
    console.error(err);
    throw new ServerErr('Failed to connect to database');
  });

  try {
    const query = {
      text: 'SELECT review_id, review.sku, review_title, review_text, review_datetime, review_rating, model, tn_image_url, username FROM review JOIN sneaker ON review.sku = ($1) JOIN customer on customer.customer_id = review.customer_id',
      values: [sku],
    };
    const res: QueryResult<ReviewInterface> = await client.query(query);
    return res.rows;
  } catch (err: any) {
    throw new ServerErr(`Unable to fetch reviews for SKU ${sku}`);
  } finally {
    await client.end();
  }
}

export async function getReviewBySkuAndCustomerId(
  sku: string,
  customerId: string
) {
  const client = getClient();
  await client.connect();

  client.on('error', (err: Error) => {
    console.error(err);
    throw new ServerErr('Failed to connect to database');
  });

  try {
    const query = {
      text: 'SELECT * FROM review WHERE sku = ($1) AND customer_id = ($2)',
      values: [sku, customerId],
    };
    const res: QueryResult<ReviewInterface> = await client.query(query);
    return res.rows;
  } catch (err: any) {
    throw new ServerErr(`Unable to fetch review for SKU ${sku}`);
  } finally {
    await client.end();
  }
}

export async function createReviewForSku(review: ReviewInterface) {
  const client = getClient();
  await client.connect();

  client.on('error', (err: Error) => {
    console.error(err);
    throw new ServerErr('Failed to connect to database');
  });

  const { sku, customer_id, review_title, review_text, review_rating } = review;

  try {
    const query = {
      text: 'INSERT INTO review (sku, customer_id, review_title, review_text, review_datetime, review_rating) VALUES ($1, $2, $3, $4, $5, $6)',
      values: [
        sku,
        customer_id,
        review_title,
        review_text,
        new Date(Date.now()),
        review_rating,
      ],
    };
    const res: QueryResult<ReviewInterface> = await client.query(query);
    return res.rows;
  } catch (err: any) {
    throw new ServerErr(`Unable to create review for SKU ${sku}`);
  } finally {
    await client.end();
  }
}
