export interface ReviewInterface {
  review_id: string;
  sku: string;
  customer_id: string;
  review_title?: string;
  review_text: string;
  review_datetime?: Date;
  review_rating: number;
  reviewer_id: string;
  model?: string;
  tnImageUrl?: string;
}
