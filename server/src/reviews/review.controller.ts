import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import {
  getReviewsForSku,
  createReviewForSku,
  getReviewBySkuAndCustomerId,
} from './review.model';
import {
  BadRequestErr,
  NotAuthenticatedErr,
  NotAuthorizedErr,
  ServerErr,
  ERR_CODES,
} from '../errors/http-errors';

export async function httpGetReviewsForSku(req: Request, res: Response) {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    return res
      .status(ERR_CODES.BAD_REQUEST)
      .json(new BadRequestErr(JSON.stringify(result.array())));
  }
  try {
    const data = await getReviewsForSku(req.params.sku);
    return res.status(200).json(data);
  } catch (err) {
    console.error(err);
    return res
      .status(ERR_CODES.SERVER_ERROR)
      .json(
        new BadRequestErr(`Unable to fetch reviews for sku: ${req.params.sku}`)
      );
  }
}

export async function httpCreateReviewForSku(req: Request, res: Response) {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    return res
      .status(ERR_CODES.BAD_REQUEST)
      .json(new BadRequestErr(JSON.stringify(result.array())));
  }
  try {
    const existingReview = await getReviewBySkuAndCustomerId(
      req.body.sku,
      req.body.customer_id
    );
    if (existingReview.length > 0) {
      return res
        .status(ERR_CODES.BAD_REQUEST)
        .json(
          new BadRequestErr(
            'Customer has already reviewed this product: ' + req.body.model
          )
        );
    }
    const data = await createReviewForSku(req.body);
    return res.status(201).json(data);
  } catch (err) {
    console.error(err);
    return res
      .status(ERR_CODES.SERVER_ERROR)
      .json(
        new ServerErr('Error in creating review for product ' + req.body.model)
      );
  }
}
