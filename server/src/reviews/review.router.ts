import { Router } from 'express';
import { body, checkSchema } from 'express-validator';
import {
  httpGetReviewsForSku,
  httpCreateReviewForSku,
} from './review.controller';
const reviewRouter = Router();

reviewRouter.get(
  '/find/:sku',
  checkSchema(
    {
      sku: { isUUID: { options: 4 }, errorMessage: 'Invalid SKU' },
    },
    ['params']
  ),
  httpGetReviewsForSku
);

reviewRouter.post(
  '/new',
  body('sku').trim(),
  body('customer_id').trim(),
  body('review_text').trim().escape(),
  body('model').trim().escape(),
  checkSchema(
    {
      sku: {
        exists: true,
        notEmpty: true,
        isUUID: { options: 4 },
        errorMessage: 'Invalid format for SKU',
      },
      customer_id: {
        exists: true,
        notEmpty: true,
        isUUID: { options: 4 },
        errorMessage: 'Invalid format for customer ID',
      },
      review_text: {
        isLength: { options: { min: 1, max: 10000 } },
        errorMessage:
          'Review text must be between 1 and 10,000 characters long',
      },
      review_rating: {
        isInt: {
          options: { min: 0, max: 5 },
        },
        errorMessage: 'Review rating must be an integer between 0 and 5',
      },
      model: {
        exists: true,
        notEmpty: true,
        errorMessage: 'Please include model name for product reviewed',
      },
    },
    ['body']
  ),
  httpCreateReviewForSku
);

export default reviewRouter;
