import { Request, Response, NextFunction } from 'express';
import { sign, verify } from 'jsonwebtoken';
import {
  BadRequestErr,
  NotAuthenticatedErr,
  NotAuthorizedErr,
  ServerErr,
  ERR_CODES,
} from '../errors/http-errors';

const SECRET = process.env.SECRET;

export function createToken(payload: any) {
  return sign(payload, SECRET!, { expiresIn: '1h' });
}

export function validateToken(token: string) {
  return verify(token, SECRET!);
}

export function checkAuthMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.method === 'OPTIONS') {
    return next();
  }

  let err: BadRequestErr | NotAuthorizedErr;

  if (!req.headers.authorization) {
    err = new BadRequestErr('"Authorization" header missing');
    return res.status(ERR_CODES.BAD_REQUEST).json({
      status: err.status,
      message: err.message,
      statusText: err.statusText,
    });
  }

  const authToken = req.headers.authorization.split(' ')[1];
  if (!authToken || authToken.length === 0) {
    err = new BadRequestErr('Invalid "Authorization" header');
    return res.status(ERR_CODES.BAD_REQUEST).json({
      status: err.status,
      message: err.message,
      statusText: err.statusText,
    });
  }

  try {
    const validatedToken = validateToken(authToken);
    next();
  } catch (err: any) {
    err = new NotAuthorizedErr('Invalid auth token');
    return res.status(ERR_CODES.NOT_AUTHORIZED).json({
      status: err.status,
      message: err.message,
      statusText: err.statusText,
    });
  }
}
