export interface UserInterface {
  customer_id: string;
  first_name: string;
  last_name: string;
}
