import { QueryResult } from 'pg';
import { getClient } from '../dbclient/db-client';
import { UserInterface } from './user.entity';

export async function getRegisteredUserByUserEmail(email: string) {
  const client = getClient();
  await client.connect();

  client.on('error', (err: Error) => {
    console.error(err);
    throw new Error(`Unable to connect to database: ${err.message}`);
  });

  try {
    const query = {
      text: 'SELECT customer_id, first_name, last_name FROM customer WHERE email_address = ($1)',
      values: [email],
    };
    const res: QueryResult<UserInterface> = await client.query(query);
    return res.rows;
  } catch (err: any) {
    throw new Error(`Unable to fetch user with email ${email}: ${err.message}`);
  } finally {
    await client.end();
  }
}
