import { Router } from 'express';
import { checkAuthMiddleware } from '../utils/auth';
import { getCurrentUser } from './user.controller';

const userRouter = Router();

userRouter.get('/current-user', checkAuthMiddleware, getCurrentUser);

export default userRouter;
