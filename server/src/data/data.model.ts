import { S3Client, GetObjectCommand } from '@aws-sdk/client-s3';
import { fromIni } from '@aws-sdk/credential-providers';
import { BadRequestErr, ServerErr } from '../errors/http-errors';

export async function getProductDescriptions(languageCode: string) {
  const client = new S3Client({
    region: 'us-east-1',
    credentials: fromIni({
      profile: 'sneakerboxx-client',
      filepath: '~/.aws/credentials',
    }),
  });
  const input = {
    Bucket: 'sneakerboxx-static-25598542',
    Key: `products-${languageCode}/product-desc-${languageCode}.json`,
  };
  const command = new GetObjectCommand(input);
  try {
    const response = await client.send(command);
    const str = await response.Body?.transformToString();
    return str;
  } catch (err: any) {
    if (err['$metadata']) {
      if (err['$metadata'].httpStatusCode === 403) {
        throw new BadRequestErr(
          `Unable to fetch data for language code: ${languageCode}`
        );
      }
    }
    throw new ServerErr('Unable to fetch product descriptions');
  }
}

export async function getHomePageTextByLanguage(languageCode: string) {
  const client = new S3Client({
    region: 'us-east-1',
    credentials: fromIni({
      profile: 'sneakerboxx-client',
      filepath: '~/.aws/credentials',
    }),
  });
  const input = {
    Bucket: 'sneakerboxx-static-25598542',
    Key: `json/${languageCode}/home-page-${languageCode}.json`,
  };
  const command = new GetObjectCommand(input);
  try {
    const response = await client.send(command);
    const str = await response.Body?.transformToString();
    return str;
  } catch (err: any) {
    if (err['$metadata']) {
      if (err['$metadata'].httpStatusCode === 403) {
        throw new BadRequestErr(
          `Unable to fetch data for language code: ${languageCode}`
        );
      }
    }
    console.error(err);
    throw new ServerErr('Unable to fetch product descriptions');
  }
}

export async function getProductDetailsPageTextByLanguage(
  languageCode: string
) {
  const client = new S3Client({
    region: 'us-east-1',
    credentials: fromIni({
      profile: 'sneakerboxx-client',
      filepath: '~/.aws/credentials',
    }),
  });
  const input = {
    Bucket: 'sneakerboxx-static-25598542',
    Key: `json/${languageCode}/product-details-page-${languageCode}.json`,
  };
  const command = new GetObjectCommand(input);
  try {
    const response = await client.send(command);
    const str = await response.Body?.transformToString();
    return str;
  } catch (err: any) {
    if (err['$metadata']) {
      if (err['$metadata'].httpStatusCode === 403) {
        throw new BadRequestErr(
          `Unable to fetch data for language code: ${languageCode}`
        );
      }
    }
    throw new ServerErr('Unable to fetch product descriptions');
  }
}

export async function getProductDescriptionBySkuAndLanguage(
  sku: string,
  languageCode: string
) {
  const client = new S3Client({
    region: 'us-east-1',
    credentials: fromIni({
      profile: 'sneakerboxx-client',
      filepath: '~/.aws/credentials',
    }),
  });
  const input = {
    Bucket: 'sneakerboxx-static-25598542',
    Key: `json/${languageCode}/product-desc-${languageCode}.json`,
  };
  const command = new GetObjectCommand(input);
  try {
    const response = await client.send(command);
    const body = await response.Body?.transformToString();
    const target = JSON.parse(body!).products.filter(
      (p: { sku: string; model: string; description: string }) => p.sku === sku
    );
    return target;
  } catch (err: any) {
    if (err['$metadata']) {
      if (err['$metadata'].httpStatusCode === 403) {
        throw new BadRequestErr(
          `Unable to fetch data for language code: ${languageCode}`
        );
      }
    }
    throw new ServerErr('Unable to fetch product descriptions');
  }
}
