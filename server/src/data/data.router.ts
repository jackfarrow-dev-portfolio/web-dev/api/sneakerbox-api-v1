import { Request, Response, Router } from 'express';
import {
  httpGetHomePageTextByLanguage,
  httpGetProductDescriptions,
  httpGetProductDescriptionBySkuAndLanguage,
  httpGetProductDetailsPageTextByLanguage,
} from './data.controller';
import { BadRequestErr, ServerErr } from '../errors/http-errors';
const dataRouter = Router();

dataRouter.get('/product-desc/:lang', async (req: Request, res: Response) => {
  try {
    const response = await httpGetProductDescriptions(req.params.lang);

    const products = response ? JSON.parse(response) : '';
    return res.status(200).json(products);
  } catch (err: any) {
    return res.status(500).json({
      message: err.message,
      status: err.status,
      statusText: err.statusText,
    });
  }
});

dataRouter.get(
  '/product-desc/:sku/:lang',
  async (req: Request, res: Response) => {
    try {
      const response = await httpGetProductDescriptionBySkuAndLanguage(
        req.params.sku,
        req.params.lang
      );

      return res.status(200).json(response);
    } catch (err: any) {
      return res.status(500).json({
        message: err.message,
        status: err.status,
        statusText: err.statusText,
      });
    }
  }
);

dataRouter.get('/home-page/:lang', async (req: Request, res: Response) => {
  try {
    const response =
      (await httpGetHomePageTextByLanguage(req.params.lang)) || '{}';
    return res.status(200).json(JSON.parse(response));
  } catch (err: any) {
    if (err instanceof BadRequestErr || err instanceof ServerErr)
      return res.status(err.status).json({
        message: err.message,
        status: err.status,
        statusText: err.statusText,
      });
  }
});

dataRouter.get(
  '/product-details-page/:lang',
  async (req: Request, res: Response) => {
    try {
      const response =
        (await httpGetProductDetailsPageTextByLanguage(req.params.lang)) ||
        '{}';
      return res.status(200).json(JSON.parse(response));
    } catch (err: any) {
      console.log('err:', err);
      if (err instanceof BadRequestErr || err instanceof ServerErr)
        return res.status(err.status).json({
          message: err.message,
          status: err.status,
          statusText: err.statusText,
        });
    }
  }
);

export default dataRouter;
