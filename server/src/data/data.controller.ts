import {
  getHomePageTextByLanguage,
  getProductDescriptions,
  getProductDescriptionBySkuAndLanguage,
  getProductDetailsPageTextByLanguage,
} from './data.model';

export async function httpGetProductDescriptions(languageCode: string) {
  return await getProductDescriptions(languageCode);
}

export async function httpGetProductDescriptionBySkuAndLanguage(
  sku: string,
  language: string
) {
  return await getProductDescriptionBySkuAndLanguage(sku, language);
}

export async function httpGetHomePageTextByLanguage(language: string) {
  return await getHomePageTextByLanguage(language);
}

export async function httpGetProductDetailsPageTextByLanguage(
  language: string
) {
  return await getProductDetailsPageTextByLanguage(language);
}
