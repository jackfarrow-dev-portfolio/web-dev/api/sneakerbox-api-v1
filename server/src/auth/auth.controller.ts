import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { getCustomerByEmailAndPassword } from '../customers/customer.model';
import { createToken } from '../utils/auth';
import { BadRequestErr, ServerErr, ERR_CODES } from '../errors/http-errors';

export async function httpLoginCustomer(req: Request, res: Response) {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    const err = new BadRequestErr(JSON.stringify(result.array()));
    return res.status(ERR_CODES.BAD_REQUEST).json({
      status: err.status,
      message: err.message,
      statusText: err.statusText,
    });
  }
  const { email, password } = req.body;
  try {
    const optionalCustomer = await getCustomerByEmailAndPassword(
      email,
      password
    );

    console.log('optionalCustomer:', optionalCustomer);
    // Customer is validated; issue token
    const customer = optionalCustomer![0];
    const jwt = createToken({
      email,
      name: `${customer.first_name} ${customer.last_name}`,
      id: customer.customer_id,
    });
    res.status(200).json(jwt);
  } catch (err: any) {
    if (err instanceof BadRequestErr || err instanceof ServerErr) {
      return res.status(err.status).json({
        status: err.status,
        message: err.message,
        statusText: err.statusText,
      });
    }
    return res.status(ERR_CODES.SERVER_ERROR).json(new ServerErr(err.message));
  }
}
