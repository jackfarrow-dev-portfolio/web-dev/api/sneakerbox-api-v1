import { Response, Request, Router } from 'express';
import { body, checkSchema } from 'express-validator';
import { httpLoginCustomer } from './auth.controller';
import { checkAuthMiddleware } from '../utils/auth';
const authRouter = Router();

authRouter.post(
  '/login',
  body('email').trim().escape(),
  body('password').trim().escape(),
  checkSchema(
    {
      // email: {
      //   isEmail: true,
      //   errorMessage: 'Invalid email format',
      // },
      email: {
        isEmpty: false,
      },
      password: {
        isEmpty: false,
      },
    },
    ['body']
  ),
  httpLoginCustomer
);

// Logout route
authRouter.get('/logout', (req: Request, res: Response) => {});

// "Secret" route
authRouter.get(
  '/secret',
  checkAuthMiddleware,
  (req: Request, res: Response) => {
    return res.status(200).json('Your personal secret value is 42!');
  }
);

authRouter.get('/hello', (req: Request, res: Response) => {
  return res.status(200).json({ message: 'hello' });
});

export default authRouter;
