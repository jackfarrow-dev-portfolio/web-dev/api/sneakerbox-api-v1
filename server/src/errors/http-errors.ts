export const ERR_CODES = {
  BAD_REQUEST: 400,
  NOT_AUTHENTICATED: 401,
  NOT_AUTHORIZED: 403,
  NOT_FOUND: 404,
  SERVER_ERROR: 500,
};

export class BadRequestErr extends Error {
  status: number;
  statusText: string | string[];

  constructor(message: string) {
    super(message);
    this.status = ERR_CODES.BAD_REQUEST;
    this.statusText = 'BAD_REQUEST';
  }
}

export class NotAuthenticatedErr extends Error {
  status: number;
  statusText: string | string[];

  constructor(message: string) {
    super(message);
    this.status = ERR_CODES.NOT_AUTHENTICATED;
    this.statusText = 'NOT_AUTHENTICATED';
  }
}

export class NotAuthorizedErr extends Error {
  status: number;
  statusText: string | string[];

  constructor(message: string) {
    super(message);
    this.status = ERR_CODES.NOT_AUTHORIZED;
    this.statusText = 'NOT_AUTHORIZED';
  }
}

export class NotFoundErr extends Error {
  status: number;
  statusText: string | string[];

  constructor(message: string) {
    super(message);
    this.status = ERR_CODES.NOT_FOUND;
    this.statusText = 'NOT_FOUND';
  }
}

export class ServerErr extends Error {
  status: number;
  statusText: string | string[];

  constructor(message: string) {
    super(message);
    this.status = ERR_CODES.SERVER_ERROR;
    this.statusText = 'SERVER_ERROR';
  }
}
