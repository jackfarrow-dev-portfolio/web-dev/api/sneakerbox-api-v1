import express, { Application, Request, Response } from 'express';
import path from 'path';
import cors from 'cors';
import helmet from 'helmet';
import sneakerRouter from './sneakers/sneaker.router';
import reviewRouter from './reviews/review.router';
import customerRouter from './customers/customer.router';
import authRouter from './auth/auth.router';
import userRouter from './user/user.router';
import dataRouter from './data/data.router';
import { NotFoundErr } from './errors/http-errors';

const app: Application = express();

app.use(express.json());
app.use(
  cors({
    origin: 'https://localhost:3000',
    methods: ['GET', 'PUT', 'POST', 'OPTIONS'],
    // credentials: true,
  })
);

app.use(helmet());
app.use('/auth', authRouter);
app.use('/sneakers', sneakerRouter);
app.use('/reviews', reviewRouter);
app.use('/customers', customerRouter);
app.use('/users', userRouter);
app.use('/data', dataRouter);
app.use('/static', (_: Request, res: Response, next) => {
  res.set('Cross-Origin-Resource-Policy', 'cross-origin');
  next();
});
app.use('/static', express.static(path.resolve(__dirname, '..', 'static')));
app.use('/*', (req: Request, res: Response) => {
  const err = new NotFoundErr(`Resource ${req.originalUrl} not found`);
  res.status(err.status).json({
    status: err.status,
    message: err.message,
    statusText: err.statusText,
  });
});

export default app;
