import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import Sneaker from './sneaker.entity';
import { getAllSneakers, getSneakerBySku } from './sneaker.model';
import { BadRequestErr, ServerErr, ERR_CODES } from '../errors/http-errors';

export async function httpGetAllSneakers(req: Request, res: Response) {
  try {
    const sneakers: Sneaker[] = await getAllSneakers();
    return res.status(200).json(sneakers);
  } catch (err: any) {
    console.error(err);
    const serverErr = new ServerErr('Unable to fetch sneakers');
    return res.status(ERR_CODES.SERVER_ERROR).json({
      status: err.status,
      message: err.message,
      statusText: err.statusText,
    });
  }
}

export async function httpGetSneakerBySku(req: Request, res: Response) {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    return res
      .status(ERR_CODES.BAD_REQUEST)
      .json(new BadRequestErr(JSON.stringify(result.array())));
  }
  try {
    const sneaker = await getSneakerBySku(req.params.sku);
    return res.status(200).json(sneaker);
  } catch (err: any) {
    return res
      .status(ERR_CODES.SERVER_ERROR)
      .json(
        new ServerErr(
          `Failed to fetch sneaker identified by SKU: ${req.params.sku}`
        )
      );
  }
}
