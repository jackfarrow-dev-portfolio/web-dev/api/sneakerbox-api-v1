export default class Sneaker {
  sku: string;
  model: string;
  sex: string;
  sizeUsMen: string[];
  sizeEuMen: string[];
  sizeUsWomen: string[];
  sizeEuWomen: string[];
  colors: string[];
  materials: string[];
  categories: string[];
  countryOfOrigin: string;
  imageUrl: string;
  tnImageUrl: string;
  tinyImageUrl: string;
  priceUsd: number;
  priceEu: number;

  constructor(sneaker: SneakerInterface) {
    const {
      sku,
      model,
      sex,
      size_us_men: sizeUsMen,
      size_eu_men: sizeEuMen,
      size_us_women: sizeUsWomen,
      size_eu_women: sizeEuWomen,
      colors,
      materials,
      categories,
      country_of_origin: countryOfOrigin,
      image_url: imageUrl,
      tn_image_url: tnImageUrl,
      tiny_image_url: tinyImageUrl,
      price_usd: priceUsd,
      price_eu: priceEu,
    } = sneaker;
    this.sku = sku;
    this.model = model;
    this.sex = sex;
    this.sizeUsMen = sizeUsMen;
    this.sizeEuMen = sizeEuMen;
    this.sizeUsWomen = sizeUsWomen;
    this.sizeEuWomen = sizeEuWomen;
    this.colors = colors;
    this.materials = materials;
    this.categories = categories;
    this.countryOfOrigin = countryOfOrigin;
    this.imageUrl = imageUrl;
    this.tnImageUrl = tnImageUrl;
    this.tinyImageUrl = tinyImageUrl;
    this.priceUsd = +priceUsd;
    this.priceEu = +priceEu;
  }
}

export interface SneakerInterface {
  sku: string;
  model: string;
  sex: string;
  size_us_men: string[];
  size_eu_men: string[];
  size_us_women: string[];
  size_eu_women: string[];
  colors: string[];
  materials: string[];
  categories: string[];
  country_of_origin: string;
  image_url: string;
  tn_image_url: string;
  tiny_image_url: string;
  price_usd: number;
  price_eu: number;
}
