import { QueryResult } from 'pg';
import Sneaker, { SneakerInterface } from './sneaker.entity';
import { getClient } from '../dbclient/db-client';
import { ServerErr } from '../errors/http-errors';

async function getAllSneakers(): Promise<Sneaker[]> {
  const client = getClient();

  await client.connect();

  client.on('error', (err: Error) => {
    throw new Error(`Unable to connect to database: ${err.message}`);
  });

  try {
    const query = {
      text: 'SELECT * FROM sneaker',
    };
    const res: QueryResult<SneakerInterface> = await client.query(query);
    return res.rows.map((sneaker: SneakerInterface) => new Sneaker(sneaker));
  } catch (err: any) {
    console.error(err);
    throw new ServerErr(`Unable to fetch sneakers`);
  } finally {
    await client.end();
  }
}

async function getSneakerBySku(sku: string): Promise<Sneaker> {
  const client = getClient();
  await client.connect();

  client.on('error', (err: Error) => {
    console.error(err);
    throw new ServerErr('Unable to connect to database');
  });

  try {
    const query = {
      text: 'SELECT * FROM sneaker WHERE sku = ($1)',
      values: [sku],
    };
    const res: QueryResult<SneakerInterface> = await client.query(query);
    const sneaker: SneakerInterface = res.rows[0];
    return new Sneaker(sneaker);
  } catch (err: any) {
    throw new ServerErr(
      `Unable to fetch sneaker identified by ${sku}: ${err.message}`
    );
  } finally {
    await client.end();
  }
}
export { getAllSneakers, getSneakerBySku };
