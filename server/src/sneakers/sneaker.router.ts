import { Request, Response, Router } from 'express';
import { checkSchema } from 'express-validator';
import { httpGetAllSneakers, httpGetSneakerBySku } from './sneaker.controller';
const sneakerRouter = Router();

sneakerRouter.get('/', httpGetAllSneakers);

sneakerRouter.get(
  '/find/:sku',
  checkSchema(
    {
      sku: { isUUID: { options: 4 }, errorMessage: 'Invalid SKU' },
    },
    ['params']
  ),
  async (req: Request, res: Response) => {
    await httpGetSneakerBySku(req, res);
  }
);

export default sneakerRouter;
