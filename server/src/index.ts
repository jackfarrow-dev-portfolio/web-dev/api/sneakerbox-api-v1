import https from 'https';
import http from 'http';
import fs from 'fs';
import path from 'path';
import 'dotenv/config';
import app from './app';

const PORT = process.env.MODE === 'prod' ? 8000 : 8001;

// const server = https.createServer(
//   {
//     key: fs.readFileSync(path.resolve(__dirname, '..', 'key.pem')),
//     cert: fs.readFileSync(path.resolve(__dirname, '..', 'cert.pem')),
//   },
//   app
// );

const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}`);
});
