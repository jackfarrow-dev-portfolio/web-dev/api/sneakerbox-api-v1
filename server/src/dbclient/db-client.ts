import { Client } from 'pg';

export function getClient() {
  const client = new Client({
    user: process.env.MODE === 'prod' ? '' : process.env.DEV_DB_USERNAME,
    host: process.env.MODE === 'prod' ? '' : process.env.DEV_DB_HOST,
    database: process.env.MODE === 'prod' ? '' : process.env.DEV_DB,
    password: process.env.MODE === 'prod' ? '' : process.env.DEV_DB_PASSWORD,
    port: process.env.MODE === 'prod' ? 0 : 5437,
  });
  return client;
}
