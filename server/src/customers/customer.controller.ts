import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { registerCustomer } from './customer.model';

export async function httpRegisterCustomer(req: Request, res: Response) {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    return res.status(400).json({ error: result.array() });
  }
  try {
    await registerCustomer(req.body);
    const customer = { ...req.body };
    delete customer.password;
    return res.status(201).json({ status: 201, customer });
  } catch (err: any) {
    return res.status(500).json({ error: 'Failed to save customer' });
  }
}
