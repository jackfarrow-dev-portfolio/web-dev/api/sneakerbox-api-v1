import bcrypt from 'bcrypt';
import { QueryResult } from 'pg';
import { getClient } from '../dbclient/db-client';
import Customer from './customer.entity';
import { BadRequestErr, ServerErr } from '../errors/http-errors';

export async function getCustomerByEmailAndPassword(
  email: string,
  password: string
) {
  const client = getClient();
  await client.connect();

  client.on('error', (err: Error) => {
    console.error(err);
    throw new ServerErr(`Unable to connect to DB: ${err.message}`);
  });

  try {
    const query = {
      text: 'SELECT * FROM customer WHERE email_address = ($1)',
      values: [email],
    };

    const customer: QueryResult<Customer> = await client.query(query);
    if (customer.rowCount === 0) {
      throw new BadRequestErr('Email/password combination not found');
    }
    const hash = customer.rows[0].password;

    const isPasswordValid = bcrypt.compareSync(password, hash);
    if (!isPasswordValid) {
      throw new BadRequestErr('Email/password combination not found');
    }

    return customer.rows;
  } catch (err: any) {
    console.log(`Unable to fetch customer with email ${email}: ${err.message}`);
    throw err;
  } finally {
    await client.end();
  }
}

export async function registerCustomer(customer: Customer) {
  const client = getClient();
  await client.connect();

  client.on('error', (err: Error) => {
    console.error(err);
    throw new Error(`Unable to connect to DB: ${err.message}`);
  });

  const {
    first_name,
    last_name,
    date_of_birth,
    username,
    billing_address_line_1,
    billing_municipality,
    billing_state_province,
    billing_country,
    billing_postal_code,
    email_address,
  } = customer;

  const preferred_language = customer.preferred_language || '';
  const billing_address_line_2 = customer.billing_address_line_2 || '';

  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(customer.password, salt);

  try {
    const query = {
      text: 'INSERT INTO customer (first_name, last_name, date_of_birth, preferred_language, username, password, billing_address_line_1, billing_address_line_2, billing_municipality, billing_state_province, billing_country, billing_postal_code, email_address) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)',
      values: [
        first_name,
        last_name,
        date_of_birth,
        preferred_language,
        username,
        hash,
        billing_address_line_1,
        billing_address_line_2,
        billing_municipality,
        billing_state_province,
        billing_country,
        billing_postal_code,
        email_address,
      ],
    };

    const res: QueryResult<Customer> = await client.query(query);
    return res.rows;
  } catch (err: any) {
    console.error(err);
    throw new ServerErr(`Failed to save customer: ${err.message}`);
  } finally {
    await client.end();
  }
}
