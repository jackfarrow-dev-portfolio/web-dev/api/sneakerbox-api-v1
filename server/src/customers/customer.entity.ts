export default interface Customer {
  customer_id?: string;
  first_name: string;
  last_name: string;
  date_of_birth: Date;
  preferred_language?: string;
  email_address: string;
  username: string;
  password: string;
  billing_address_line_1: string;
  billing_address_line_2: string;
  billing_municipality: string;
  billing_state_province: string;
  billing_country: string;
  billing_postal_code: string;
}
