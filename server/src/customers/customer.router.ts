import { Router } from 'express';
import { body, checkSchema } from 'express-validator';
import { httpRegisterCustomer } from './customer.controller';

const todayMinus18Years = new Date(
  Date.now() - 1000 * 60 * 60 * 24 * 365.25 * 18
);
const customerRouter = Router();

customerRouter.post(
  '/create',
  body('first_name').trim().escape(),
  body('last_name').trim().escape(),
  body('username').trim().escape(),
  body('password').trim().escape(),
  body('billing_address_line_1').trim().escape(),
  body('billing_municipality').trim().escape(),
  body('billing_state_province').trim().escape(),
  body('billing_country').trim().escape(),
  body('billing_postal_code').trim().escape(),
  checkSchema(
    {
      first_name: {
        isEmpty: false,
        isAlphanumeric: true,
        errorMessage: 'Invalid customer first name',
      },
      last_name: {
        isEmpty: false,
        isAlphanumeric: true,
        errorMessage: 'Invalid customer last name',
      },
      /*
      TODO: 2024-01-14 DOB is required by db schema; enforce
      as required field here
      */
      // date_of_birth: {
      //   isDate: { options: { delimiters: ['/', '-'] } },
      //   // isBefore: { options: [todayMinus18Years] },
      //   errorMessage: 'Date of birth must be valid date at least 18 years ago',
      // },
      email_address: {
        isEmail: true,
        errorMessage: 'Invalid email address format',
      },
      username: {
        isEmpty: false,
        isAlphanumeric: true,
        isLength: { options: { min: 8, max: 50 } },
        errorMessage:
          'Username must be at least 8 and no more than 50 characters',
      },
      password: {
        isStrongPassword: {
          options: {
            minLength: 8,
            minLowercase: 1,
            minUppercase: 1,
            minNumbers: 1,
            minSymbols: 1,
          },
        },
      },
      billing_address_line_1: { isEmpty: false },
      billing_municipality: { isEmpty: false },
      billing_state_province: { isEmpty: false },
      /*
      TODO: 2024-01-14 billing_country should be required,
      but db save is allowed without it (testing from Postman)
      */
      billing_country: { isEmpty: false },
      billing_postal_code: { isEmpty: false },
    },
    ['body']
  ),
  httpRegisterCustomer
);

export default customerRouter;
